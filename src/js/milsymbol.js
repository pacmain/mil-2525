import * as ms from 'milsymbol';

export function symbol(sidc, options){
    return new ms.Symbol(sidc,options)
}

export function asSVG(sidc, options){
    return symbol(sidc, options).asSVG();
}

export function asSVGUrl(sidc, options){
    return symbol(sidc, options).toDataURL();
}

export function asPNGUrl(sidc, options){
    return symbol(sidc, options).asCanvas().toDataURL();
}