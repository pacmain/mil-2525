export const context = [
    {id: "0", text: "Reality", value: {"idx": 2, "value": "0"}},
    {id: "1", text: "Exercise", value: {"idx": 2, "value": "1"}},
    {id: "2", text: "Simulation", value: {"idx": 2, "value": "2"}}
]
export const standard_identity = [
    {id: "0", text: "Friend", value: {"idx": 3, "value": "3"}},
    {id: "1", text: "Pending", value: {"idx": 3, "value": "0"}},
    {id: "2", text: "Unknown", value: {"idx": 3, "value": "1"}},
    {id: "3", text: "Assumed Friend", value: {"idx": 3, "value": "2"}},
    {id: "4", text: "Neutral", value: {"idx": 3, "value": "4"}},
    {id: "5", text: "Suspect/Joker", value: {"idx": 3, "value": "5"}},
    {id: "6", text: "Hostile/Faker", value: {"idx": 3, "value": "6"}},
]

export const symbol_set = [
    {id: "0", text: "Land Unit", value: {"idx": 4, "value": "10"}},
    {id: "1", text: "Unknown", value: {"idx": 4, "value": "00"}},
    {id: "2", text: "Space", value: {"idx": 4, "value": "05"}},
    {id: "3", text: "Air", value: {"idx": 4, "value": "01"}},
    {id: "4", text: "Land Equipment and Sea Surface", value: {"idx": 4, "value": "15"}},
    {id: "5", text: "Land Installation", value: {"idx": 4, "value": "20"}},
    {id: "6", text: "Activity/Event", value: {"idx": 4, "value": "40"}},
]

export const status = [
    {id: "0", text: "Present", value: {"idx": 6, "value": "0"}},
    {id: "1", text: "Planned/Anticipated/Suspect", value: {"idx": 6, "value": "1"}},
    {id: "2", text: "Present/Fully capable", value: {"idx": 6, "value": "2"}},
    {id: "3", text: "Present/Damaged", value: {"idx": 6, "value": "3"}},
    {id: "4", text: "Present/Destroyed", value: {"idx": 6, "value": "4"}},
    {id: "5", text: "Present/Full to capacity", value: {"idx": 6, "value": "5"}},
]

export const headquarters_task_force_dummy = [
    {id: "0", text: "Unknown", value: {"idx": 7, "value": "0"}},
    {id: "1", text: "Feint/Dummy", value: {"idx": 7, "value": "1"}},
    {id: "2", text: "Headquarters", value: {"idx": 7, "value": "2"}},
    {id: "3", text: "Feint/Dummy Headquarters", value: {"idx": 7, "value": "3"}},
    {id: "4", text: "Task Force", value: {"idx": 7, "value": "4"}},
    {id: "5", text: "Feint/Dummy Task Force", value: {"idx": 7, "value": "5"}},
    {id: "6", text: "Task Force Headquarters", value: {"idx": 7, "value": "6"}},
    {id: "7", text: "Feint/Dummy Task Force Headquarters", value: {"idx": 7, "value": "7"}},
]

export const echelon_mobility_Towed_array_amplifier = [
    {id: "0", text: "Unknown", value: {"idx": 8, "value": "00"}},
    {id: "1", text: "Team/Crew ", value: {"idx": 8, "value": "11"}},
    {id: "2", text: "Squad", value: {"idx": 8, "value": "12"}},
    {id: "3", text: "Section", value: {"idx": 8, "value": "13"}},
    // {id: "4", text: "Task Force", value: {"idx": 8, "value": "4"}},
    // {id: "5", text: "Feint/Dummy Task Force", value: {"idx": 8, "value": "5"}},
    // {id: "6", text: "Task Force Headquarters", value: {"idx": 8, "value": "6"}},
    // {id: "7", text: "Feint/Dummy Task Force Headquarters", value: {"idx": 8, "value": "7"}},
]